package com.task19_Servlets;


import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/confirm")
public class ConfirmOrder extends HttpServlet {


  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException{
    resp.setContentType("text/html");
    PrintWriter out = resp.getWriter();
    out.println("<html><head><body>");
    out.println("<h2>Order has been confirmed</h2>");
    com.task19_Servlets.OrderedPizza.clearOrders();
    out.println("<a href=\"orders\">Back to menu</a>");
    out.println("</body></html>");

  }

}
