package com.task19_Servlets.domain;

public enum Pizza {
  MARGARITA, VEGETARIAN, BACON_BOMB, CHEESE_OCEAN
}
