package com.task19_Servlets.domain;

public class Order {

  private int id;
  private Pizza pizza;

  public Order(Pizza pizza, int id){
    this.pizza = pizza;
    this.id = id;
  }

  public int getId() {
    return id;
  }

  public Pizza getPizza() {
    return pizza;
  }

  @Override
  public String toString() {
    return "Order{" +
        "id=" + id +
        ", pizza=" + pizza +
        '}';
  }
}
