package com.task19_Servlets;

import com.task19_Servlets.domain.Order;
import com.task19_Servlets.domain.Pizza;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebServlet("/orders/*")
public class OrderedPizza extends HttpServlet {

  private static Logger logger1 = LogManager.getLogger(OrderedPizza.class);
  private static Map<Integer, Order> orders = new HashMap<>();

  public static void clearOrders() {
    orders.clear();
  }

  @Override
  public void init() throws ServletException {
  logger1.info("Servlet " + this.getServletName() + " has started");
  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
  resp.setContentType("text/html");
  PrintWriter out = resp.getWriter();
    out.println("<html><head><body>");
    out.println("<p> <a href='orders'>REFRESH</a> </p>");
    out.println("<h2>Menu</h2>");
    ArrayList<Pizza> all = new ArrayList<>(EnumSet.allOf(Pizza.class));
    for (Pizza pizza : all) {
      out.println("<p>" + String.valueOf(pizza).substring(0,1) +
          String.valueOf(pizza).substring(1).toLowerCase() + "</p>");
    }
    out.println("<h2>Your order is:</h2>");
    for (Order order : orders.values()) {
      out.println("<p>Pizza #: " + order.getId() + "</p>");
      out.println("<p>" + String.valueOf(order.getPizza()).substring(0, 1) +
          String.valueOf(order.getPizza()).substring(1).toLowerCase() + "</p>");
    }
    out.println("<form action='orders' method='POST'>\n"
        + "  Add pizza: <input type='text' name='order'>\n"
        + "  <button type='submit'>Add pizza to order</button>\n"
        + "</form>");
    out.println("<form>\n"
        + "  <p><b>Delete pizza</b></p>\n"
        + "  <p>Pizza #: <input type='text' name='pizza_id'>\n"
        + "    <input type='button' onclick='remove(this.form.pizza_id.value)' name='ok' value='Delete'/>\n"
        + "  </p>\n"
        + "</form>");
    out.println("<form action='confirm' method='POST'>\n"
        + "  Add pizza: <input type='text' name='confirm'>\n"
        + "  <button type='submit'>Confirm order</button>\n"
        + "</form>");
    out.println("<script type='text/javascript'>\n"
        + "  function remove(id) { fetch('orders/' + id, {method: 'DELETE'}); }\n"
        + "</script>");
    out.println("</body></html>");
  }
  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException{
    String orderName = req.getParameter("order");
    Order newOrder = new Order(Pizza.valueOf(orderName.toUpperCase()), orders.size() + 1);
    orders.put(newOrder.getId(), newOrder);
    doGet(req,resp);
  }

  @Override
  protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
      throws ServletException, IOException{
    String id = req.getRequestURI();
    id = id.replace("/task19_Servlets-1.0/orders/", "");
    orders.remove(Integer.parseInt(id));
  }

  @Override
  public void destroy() {
    logger1.info("Servlet " + this.getServletName() + " has stopped");
  }
}

