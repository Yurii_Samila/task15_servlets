//package com.task19_Servlets;
//
//import com.task19_Servlets.domain.Pizza;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.EnumSet;
//import java.util.Optional;
//import javax.servlet.Filter;
//import javax.servlet.FilterChain;
//import javax.servlet.FilterConfig;
//import javax.servlet.ServletException;
//import javax.servlet.ServletRequest;
//import javax.servlet.ServletResponse;
//import javax.servlet.annotation.WebFilter;
//import javax.servlet.http.HttpServletResponse;
//
//@WebFilter("/orders/*")
//public class OrderFilter implements Filter {
//
//  @Override
//  public void init(FilterConfig filterConfig) throws ServletException {
//  }
//
//  @Override
//  public void doFilter(ServletRequest req, ServletResponse res,
//      FilterChain filterChain) throws IOException, ServletException {
//    Optional<String> order = Optional.ofNullable(req.getParameter("order"));
//    ArrayList<Pizza> all = new ArrayList<>(EnumSet.allOf(Pizza.class));
//    for (Pizza pizza : all) {
//      if (order.filter(o -> o.equalsIgnoreCase(String.valueOf(pizza))).isPresent()){
//        ((HttpServletResponse) res).sendError(403, "Please, choose correct pizza");
//      }
//        filterChain.doFilter(req, res);
//    }
//    }
//  }

